class Calculate

	itemHaveDiscount = true

	def initialize(value, quantity, value_discount)
		@value = value
		@quantity = quantity
		@value_discount = value_discount
	end

	
	def calculate_total
	
		total_value = @quantity  * @value
	end
	
	def discount
		
		discount = (@value_discount/100) * @value
	end

	def total_with_discount
	
		if (itemHaveDiscount)
			
			new_total = calculate_total	- discount
		else
			
			calculate_total
			
		end
	end
end