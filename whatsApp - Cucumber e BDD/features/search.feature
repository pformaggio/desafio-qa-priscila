# encoding: utf-8
# language: en


Feature: Search
	As a WhatsApp user
	I should be able to search a contact
	So I can find more easily the message
	
	
	Scenario: Search a contact
			Given i click in the search icon
			And i fill the "<name>" i am looking for
			And i click in the result
			Then i can see the messages from this person