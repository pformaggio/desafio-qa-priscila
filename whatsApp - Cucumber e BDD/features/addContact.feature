# encoding: utf-8
# language: en


Feature: Add new contact
	As a WhatsApp user
	I should be able to add a celphone number
	So I can see who is calling for me

		Scenario: Contact list
			Given i am a new user
			When i click contact list
			Then i can see all my contact numbers



		Scenario: Add new number
			Given i am a new user
			And i click contact list 
			And i click add button
			When i fill the "<name>" and "<celphone number>"
			And i click save button
			Then i have a new contact in my list

